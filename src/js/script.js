let btn1 = document.querySelector('#v1');
let btn2 = document.querySelector('#v2');

let one = document.querySelector('.v1s');
let two = document.querySelector('.v2s');

let btnVersion = document.querySelector('#version');
let ves = document.querySelector('.ves');

btn1.onclick = function(){

    one.classList.remove('hide');
    if(one){
        two.classList.add('hide');
        btn1.classList.add('active');
        btn2.classList.remove('active')
    }

};

btn2.onclick = function(){

    two.classList.remove('hide');
    if(two){
        one.classList.add('hide');
        btn2.classList.add('active');
        btn1.classList.remove('active')
    }

};

btnVersion.onclick = function(){

    ves.classList.toggle("hide");

};